//Example program

function Start() {

    let meshSet = [];
    //Body
    meshSet.push(new3DRect(0, 0, 0, 10, 10, 10, "hsl(120,100%,50%,0.2)"));
    meshSet.push(new3DRect(0, 0, 0, 7, 7, 7, "hsl(120,100%,50%,0.2)"));
    //Hat
    meshSet.push(new3DRect(0, -11, 0, 14, 14, 1, "hsl(43, 45%, 70%)"));
    meshSet.push(new3DRect(0, -14, 0, 7, 7, 1, "hsl(50, 48%, 72%)"));
    //Eyes
    meshSet.push(new3DRect(0 - 5, 0, -10, 1, .1, 3, "hsla(0, 45%, 0%, 0.2)"));
    meshSet.push(new3DRect(0 + 5, 0, -10, 1, .1, 3, "hsla(0, 45%, 0%, 0.2)"));
    //Blush
    meshSet.push(new3DRect(0 - 5, 5, -10, 1, .1, .5, "hsla(0, 100%, 50%, 0.5)"));
    meshSet.push(new3DRect(0 + 5, 5, -10, 1, .1, .5, "hsla(0, 100%, 50%, 0.5)"));
    tris.push(new Fig(meshSet, new Point3D(0, -20, 0)));
    // tris.push(new Text3D("Slimebrero", 0, -20, 0, "#FFF"));

    for (let i = 0; i < 50; i++) {
        let x = rRange(-100, 100);
        let y = rRange(-100, 100);
        let z = rRange(-100, 100);
        let color = Choose(0, 240);
        tris.push(new3DRect(x, y, z, 2, 2, 2, "hsla(" + color + ",100%,80%,0.2)"));
        tris.push(new3DRect(x, y, z, .5, .5, .5, "hsla(0,100%,100%,1)"));
    }
    playerSpeed = 20;
    renderDist = 500;
}
let y = 0;


function Update() {
    y += 0.075;
    ctx.fillStyle = "#000011";
    ctx.fillRect(-c.width / 2, -c.height / 2, c.width, c.height);

    tris[0].y = -Math.abs(Math.sin(y)) * 15;



}