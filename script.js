let c = document.getElementById("c");
let ctx = c.getContext("2d");
c.width = window.innerWidth;
c.height = window.innerHeight;
ctx.translate(c.width / 2, c.height / 2);
let FOV = 400;

let keys = [];
document.onkeydown = e => keys[e.key] = true;
document.onkeyup = e => keys[e.key] = false;

let mouse = {
    x: 0,
    y: 0,
    button: [],
    isPressed: false,
    old: {
        x: this.x,
        y: this.y
    }
};
document.onmousedown = e => {
    mouse.old.x = e.clientX;
    mouse.old.y = e.clientY;
    camera.ozxA = camera.zxA;
    camera.ozyA = camera.zyA;

    mouse.isPressed = true;
    mouse.button[e.button] = true;
};
document.onmouseup = e => {
    mouse.isPressed = false;
    mouse.button[e.button] = false;
};
document.onmousemove = e => {
    mouse.x = e.clientX - c.getBoundingClientRect().left;
    mouse.y = e.clientY - c.getBoundingClientRect().top;
};
document.oncontextmenu = () => {
    return false;
};

c.requestPointerLock = c.requestPointerLock ||
    c.mozRequestPointerLock;

document.exitPointerLock = document.exitPointerLock ||
    document.mozExitPointerLock;

c.onclick = function () {
    c.requestPointerLock();
};
document.addEventListener('pointerlockchange', lockChangeAlert, false);
document.addEventListener('mozpointerlockchange', lockChangeAlert, false);

function lockChangeAlert() {
    if (document.pointerLockElement === c ||
        document.mozPointerLockElement === c) {
        console.log('The pointer lock status is now locked');
        document.addEventListener("mousemove", updatePosition, false);
    } else {
        console.log('The pointer lock status is now unlocked');
        document.removeEventListener("mousemove", updatePosition, false);
    }
}
var animation;

function updatePosition(e) {
    camera.zxA += e.movementX / (1000 / sensitivity);
    camera.zyA += e.movementY / (1000 / sensitivity);

}

let camera = {
    x: 0,
    y: 0,
    z: -30,
    velocity: {
        x: 0,
        y: 0,
        z: 0
    },
    canJump: true,
    zxA: 0,
    zyA: 0,
    ozxA: 0,
    ozyA: 0,
    zoom: 1
};

function copy(obj) {
    var a = {};
    for (var x in obj) a[x] = obj[x];
    return a;
}

function rRange(beginning, end) {
    if (end) {
        let no1 = Math.min(beginning, end);
        let no2 = Math.max(beginning, end);
        let diff = no2 - no1;
        return (Math.random() * diff) + no1;
    } else {
        return Math.random() * beginning;
    }
}

function average3D(array) {
    let x = 0;
    let y = 0;
    let z = 0;
    for (let i = 0; i < array.length; i++) {
        x += array[i].x;
        y += array[i].y;
        z += array[i].z;
    }
    x /= array.length;
    y /= array.length;
    z /= array.length;
    return new Point3D(x, y, z);
}


function dist3D(p1, p2) {
    let dist = Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2) + Math.pow(p1.z - p2.z, 2));
    return dist;
}

class Point3D {
    constructor(x, y, z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
class Text {
    constructor(position, data) {
        this.position = position;
        this.data = data;
    }
    render() {
        //literally nothing here?
    }
}

function Rotate(x, y, camX, camY, angle) {
    let x1 = x;
    let y1 = y;
    let x2 = (x1 - camX) * Math.cos(angle) - (y1 - camY) * Math.sin(angle) + camX;
    let y2 = (y1 - camY) * Math.cos(angle) + (x1 - camX) * Math.sin(angle) + camY;
    return [x2, y2];
}

function Choose(op1, op2) {
    if (Math.random() > 0.5) {
        return op1;
    } else {
        return op2;
    }
}

function Render(Point3D) {
    let x = Point3D.x;
    let y = Point3D.y;
    let z = Point3D.z;

    //ZX rotation
    let temp = Rotate(x, z, camera.x, camera.z, camera.zxA);
    x = temp[0];
    z = temp[1];
    //ZX rotation
    temp = Rotate(y, z, camera.y, camera.z, camera.zyA);
    y = temp[0];
    z = temp[1];




    z = Math.max(camera.z, z);
    let f = (z - camera.z) / FOV;

    x = (x - camera.x) / f;
    y = (y - camera.y) / f;
    return [x * camera.zoom, y * camera.zoom];
}

// let points = [];
let tris = [];
class Tri {
    constructor(pointSet, colour, offset = new Point3D(0, 0, 0)) {
        this.pointSet = pointSet; //For temporary copying
        this.points = [];
        this.colour = colour;
        for (let i = 0; i < 3; i++) {
            let temp = new Point3D(pointSet[i][0], pointSet[i][1], pointSet[i][2])
            this.points.push(temp);
        }
        this.offset = offset;
    }
    render() {
        if (this.getDist() > 2 && this.getZDist() > 0)
            for (let i = 0; i < this.points.length; i++) {
                let temp = copy(this.points[i]);
                temp.x += this.offset.x;
                temp.y += this.offset.y;
                temp.z += this.offset.z;
                renderedPoints.push([Render(temp), this.colour, "tri"]);
            }
    }
    getDist() {
        let d1 = dist3D(this.points[0], camera);
        let d2 = dist3D(this.points[1], camera);
        let d3 = dist3D(this.points[2], camera);


        let dist = (d1 + d2 + d3) / 3;
        return dist;
    }
    getZDist() {
        let p1 = copy(this.points[0]);
        let p2 = copy(this.points[1]);
        let p3 = copy(this.points[2]);
        //ZX Rotation
        let rotated = Rotate(p1.z, p1.x, camera.x, camera.y, camera.zxA);
        p1.z = rotated[0];
        p1.x = rotated[1];
        rotated = Rotate(p2.z, p2.x, camera.x, camera.y, camera.zxA);
        p2.z = rotated[0];
        p2.x = rotated[1];
        rotated = Rotate(p3.z, p3.x, camera.x, camera.y, camera.zxA);
        p3.z = rotated[0];
        p3.x = rotated[1];
        //ZY Rotation
        rotated = Rotate(p1.z, p1.y, camera.x, camera.y, camera.zyA);
        p1.z = rotated[0];
        p1.y = rotated[1];
        rotated = Rotate(p2.z, p2.y, camera.x, camera.y, camera.zyA);
        p2.z = rotated[0];
        p2.y = rotated[1];
        rotated = Rotate(p3.z, p3.y, camera.x, camera.y, camera.zyA);
        p3.z = rotated[0];
        p3.y = rotated[1];
        let d1 = dist3D(p1, camera);
        let d2 = dist3D(p2, camera);
        let d3 = dist3D(p3, camera);


        let dist = (d1 + d2 + d3) / 3;
        return dist;
    }
}
class Mesh {
    constructor(trisSet, center) {
        this.tris = trisSet;
        // let averagedSet = [];
        // for (let i = 0; i < trisSet.length; i++) {
        //     for (let j = 0; j < trisSet[i].points.length; j++) {
        //         averagedSet[i * trisSet[i].points.length + j] = average3D(trisSet[i].points)
        //     }
        // }
        this.x = center.x;
        this.y = center.y;
        this.z = center.z;
    }
    render() {
        for (let i = 0; i < this.tris.length; i++) {
            let temp = new Tri(this.tris[i].pointSet, this.tris[i].colour);
            temp.offset.x += this.x;
            temp.offset.y += this.y;
            temp.offset.z += this.z;
            temp.render();
        }
    }
    rotate(origin, angleZX, angleZY, angleXY) { //UNFINISHED, DO NOT USE
        for (let i = 0; i < this.tris.length; i++) {
            this.tris[i].rotate(origin, angleZX, angleZY, angleXY);
        }
    }
    getDist() {
        let center = new Point3D(this.x, this.y, this.z);
        return dist3D(center, camera);
    }
}
//I know this looks exactly like Meshes but its used to move multiple meshes, like how you can move multiple tris in a mesh
class Fig {
    constructor(meshes, center) {
        this.meshes = meshes;
        this.x = center.x;
        this.y = center.y;
        this.z = center.z;
    }
    render() {
        for (let i = 0; i < this.meshes.length; i++) {
            let center = new Point3D(this.meshes[i].x, this.meshes[i].y, this.meshes[i].z)
            let temp = new Mesh(this.meshes[i].tris, center);
            temp.x += this.x;
            temp.y += this.y;
            temp.z += this.z;
            temp.render();
        }
    }
    getDist() {
        let center = new Point3D(this.x, this.y, this.z);
        return dist3D(center, camera);
    }
}

function new3DRect(x, y, z, width, height, length, colour) {
    let trisSet = [];

    //Sides
    trisSet.push(new Tri([
        [-width, -length, -height],
        [-width, -length, height],
        [-width, length, height]
    ], colour));
    trisSet.push(new Tri([
        [-width, -length, -height],
        [-width, length, height],
        [-width, length, -height]
    ], colour));
    trisSet.push(new Tri([
        [width, -length, height],
        [width, -length, -height],
        [width, length, -height]
    ], colour));
    trisSet.push(new Tri([
        [width, -length, height],
        [width, length, -height],
        [width, length, height]
    ], colour));

    //Front/Back
    trisSet.push(new Tri([
        [width, -length, -height],
        [-width, -length, -height],
        [-width, length, -height]
    ], colour));
    trisSet.push(new Tri([
        [width, -length, -height],
        [-width, length, -height],
        [width, length, -height]
    ], colour));
    trisSet.push(new Tri([
        [-width, -length, height],
        [width, -length, height],
        [width, length, height]
    ], colour));
    trisSet.push(new Tri([
        [-width, -length, height],
        [width, length, height],
        [-width, length, height]
    ], colour));

    //Top/Bottom
    trisSet.push(new Tri([
        [-width, -length, height],
        [-width, -length, -height],
        [width, -length, -height]
    ], colour));
    trisSet.push(new Tri([
        [-width, -length, height],
        [width, -length, height],
        [width, -length, -height]
    ], colour));
    trisSet.push(new Tri([
        [width, length, height],
        [width, length, -height],
        [-width, length, -height]
    ], colour));
    trisSet.push(new Tri([
        [width, length, height],
        [-width, length, height],
        [-width, length, -height]
    ], colour));
    return new Mesh(trisSet, new Point3D(x, y, z));
}

class Text3D {
    constructor(txt, x, y, z, colour) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.txt = txt;
        this.colour = colour;
    }
    render() {
        renderedPoints.push([Render(new Point3D(this.x, this.y, this.z)), this.colour, "txt", this.txt]);
    }
    getDist() {
        return dist3D(new Point3D(this.x, this.y, this.z), camera);
    }
    getZDist() {
        let p = new Point3D(this.x, this.y, this.z);
        let rotated = Rotate(p.z, p.x, camera.x, camera.z, -camera.zxA);
        p.x = rotated.y;
        p.z = rotated.x;
        rotated = Rotate(p.z, p.y, camera.y, camera.z, -camera.zyA);
        p.y = rotated.y;
        p.z = rotated.x;
        return dist3D(p, camera);
    }
}

let renderedPoints = [];
let sortedPoints = [];
ctx.font = "30px Arial";
let gravity = 1;
let jumpPower = 3;
let arrowSpeed = 4;
let playerSpeed = 5;
let sensitivity = 10;
let renderDist = 100;
let zoomLevel = 3;
let allowZoom = true;
Start();

function Loop() {
    ctx.clearRect(-c.width / 2, -c.height / 2, c.width, c.height);
    if (camera.canJump) {
        if (keys[" "]) {
            camera.velocity.y = -jumpPower;
            camera.canJump = false;
        }
    } else if (camera.y >= 0) {
        camera.velocity.y = 0;
        camera.y = 0;
        camera.canJump = true;
    } else if (camera.y != 0) {
        camera.velocity.y += gravity / 10;
    }

    camera.x += camera.velocity.x;
    camera.y += camera.velocity.y;
    camera.z += camera.velocity.z;


    if (keys["ArrowUp"]) {
        camera.zyA -= arrowSpeed / 100;
    }
    if (keys["ArrowDown"]) {
        camera.zyA += arrowSpeed / 100;
    }
    if (keys["ArrowLeft"]) {
        camera.zxA -= arrowSpeed / 100;
    }
    if (keys["ArrowRight"]) {
        camera.zxA += arrowSpeed / 100;
    }
    if (keys["w"]) {
        camera.x += Math.sin(camera.zxA) * playerSpeed / 10;
        camera.z += Math.cos(camera.zxA) * playerSpeed / 10;
    }
    if (keys["s"]) {
        camera.x -= Math.sin(camera.zxA) * playerSpeed / 10;
        camera.z -= Math.cos(camera.zxA) * playerSpeed / 10;
    }
    if (keys["a"]) {
        camera.x -= Math.cos(camera.zxA) * playerSpeed / 10;
        camera.z += Math.sin(camera.zxA) * playerSpeed / 10;
    }
    if (keys["d"]) {
        camera.x += Math.cos(camera.zxA) * playerSpeed / 10;
        camera.z -= Math.sin(camera.zxA) * playerSpeed / 10;
    }
    if (mouse.button[2] && allowZoom) {
        camera.zoom = zoomLevel;
    } else {
        camera.zoom = 1;
    }
    Update();
    camera.zyA = Math.min(Math.PI / 2, Math.max(camera.zyA, -Math.PI / 2));
    if (Math.abs(camera.zxA) > Math.PI) {
        if (camera.zxA < 0) {
            camera.zxA += 2 * Math.PI;
        } else if (camera.zxA > 0) {
            camera.zxA -= 2 * Math.PI;
        }
    }
    for (let i = 0; i < tris.length; i++) {
        sortedPoints[i] = tris[i];
    }
    sortedPoints.sort((a, b) => b.getDist() - a.getDist());
    for (let i = renderedPoints.length; i > 0; i--) {
        renderedPoints.pop();
    }
    for (let i = 0; i < tris.length; i++) {
        if (sortedPoints[i].getDist() < renderDist && sortedPoints[i].getDist() > 10) {
            sortedPoints[i].render();
        }
    }

    for (let i = 0; i < renderedPoints.length; i++) {
        // let dist = tris[i].getDist();
        let p = renderedPoints;
        if (p[i][2] == "tri") {
            let p1 = p[i];
            let p2 = p[i + 1];
            let p3 = p[i + 2];
            ctx.fillStyle = p1[1];
            ctx.beginPath();
            ctx.moveTo(p1[0][0], p1[0][1]);
            ctx.lineTo(p2[0][0], p2[0][1]);
            ctx.lineTo(p3[0][0], p3[0][1]);
            ctx.lineTo(p1[0][0], p1[0][1]);
            ctx.fill();
            i += 2;
        } else if (p[i][2] == "txt") {
            let p1 = p[i];
            let size = ctx.measureText(p1[3]);
            // ctx.font = (100 - tris[i / 3].getDist() / 1) + "px Arial";
            ctx.fillStyle = p1[1];
            ctx.fillText(p1[3], p1[0][0] - size.width / 2, p1[0][1]);
        }
    }
    requestAnimationFrame(Loop);
}
Loop();