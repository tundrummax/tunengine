# This is a tutorial on how to use Tungine!
First of all, make sure you have unzipped your files, and open the folder in your code editor of choice.

The language that Tungine uses is JavaScript, as I am used to it and I can quite easily test my program without problem.

To start, open World.js. This is the file that stores **your** code.

You will see something like this when you open it.

![Imgur](https://i.imgur.com/u4G92XV.png)

'Start' is where you will place your initialisation code, such as initialising rectangles, tris etc.

'Update' is where you will write your game loop, in here you will be able to run a single piece of code multiple times.

TEMP:
At this current moment, you will see example code.

![Imgur](https://i.imgur.com/CEIgkFM.png)

If you open 'index.html' in your browser of choice, you will see this cutie.

![Imgur](https://i.imgur.com/jye0wn1.png)

Along with 500 triangles randomly placed next to it.

BACK TO THE TUTORIAL:
To start, write 'new3DRect()', this will begin creating a rectangular prism.
It has many parameters, such as:

* x
* y
* z
* width
* height
* length
* colour

With those parameters, you can create a cuboid of any size, anywhere.